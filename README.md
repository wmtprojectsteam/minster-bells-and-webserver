# Minster Bells and Webserver

This repository contains the documentation for the Minster Bells and Webserver. Note that the Bells and Music Player was extensively upgraded and updated over the Winter Maintenance Period 2019/20 and so the documents are provided here for archival purposes.  The latest documentation for the Bells and Music Player may be found in the minster-bells-docs repository and the latest documentation for the Webserver may be found in the webserver-docs repository.
